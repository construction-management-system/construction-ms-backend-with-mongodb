const Customer = require("../models/users/customer");
const Project = require("../models/projects/project");
const validationResult = require("../utils/CheckValidationResult");

module.exports.register = async (req, res) => {
  const errors = validationResult(req);
  if (errors) return res.status(400).json({ errors });
  const {
    first_name,
    last_name,
    ssn,
    photo,
    email,
    phone,
    country,
    city,
    district,
    home_number,
    projects,
    contract_file,
  } = req.body;

  let customer = new Customer({
    first_name,
    last_name,
    ssn,
    photo,
    contact: {
      email,
      phone,
      address: { country, city, district, home_number },
    },
    projects,
    contract_file,
  });

  customer = await customer.save();

  return res.json(customer);
};

module.exports.getCustomers = async (req, res) => {
  let customers = await Customer.find().sort("updatedAt").populate("projects");
  if (!customers) return res.state(400).json({ error: "No customers found" });
  return res.json(customers);
};

module.exports.getCustomer = async (req, res) => {
  const { customer_id } = req.params;
  let customer = await Customer.findById(customer_id);
  if (!customer) return res.state(400).json({ error: "invalid customer" });
  return res.json(customer);
};

module.exports.update = async (req, res) => {
  const {
    first_name,
    last_name,
    ssn,
    photo,
    email,
    phone,
    country,
    city,
    district,
    home_number,
    projects,
    contract_file,
  } = req.body;
  const { customer_id } = req.params;

  let project;
  for (element of projects) {
    project = await Project.findById(element);
    if (!project) return res.state(400).json({ error: "invalid project" });
  }

  let customer = await Customer.findById(customer_id);
  if (!customer) return res.state(400).json({ error: "invalid customer" });

  customer = await Customer.findByIdAndUpdate(
    customer_id,
    {
      first_name,
      last_name,
      ssn,
      photo,
      contact: {
        email,
        phone,
        address: { country, city, district, home_number },
      },
      projects,
      contract_file,
    },
    { new: true }
  );

  if (!customer)
    return res
      .state(404)
      .json({ error: `can't update the customer with the given id` });

  return res.json(customer);
};

module.exports.remove = async (req, res) => {
  const { customer_id } = req.params;
  const customer = await Customer.findByIdAndRemove(customer_id);
  if (!customer) return res.state(400).json({ error: "invalid customer" });

  return res.json(customer);
};
