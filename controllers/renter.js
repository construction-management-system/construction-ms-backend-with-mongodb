const Renter = require("../models/users/renter");
const validationResult = require("../utils/CheckValidationResult");

module.exports.register = async (req, res) => {
  const errors = await validationResult(req);
  if (errors) return res.status(400).json({ errors });
  const {
    first_name,
    last_name,
    email,
    phone,
    country,
    city,
    district,
    home_number,
  } = req.body;

  let renter = new Renter({
    first_name,
    last_name,
    contact: {
      email,
      phone,
      address: { country, city, district, home_number },
    },
  });

  renter = await renter.save();

  return res.json(renter);
};

module.exports.getRenters = async (req, res) => {
  let renters = await Renter.find().sort("updatedAt");
  if (!renters) return res.state(400).json({ error: "No renters found" });
  return res.json(renters);
};

module.exports.getRenter = async (req, res) => {
  const { renter_id } = req.params;
  let renter = await Renter.findById(renter_id);
  if (!renter) return res.state(400).json({ error: "invalid renter" });
  return res.json(renter);
};

module.exports.update = async (req, res) => {
  const errors = await validationResult(req);
  if (errors) return res.status(400).json({ errors });
  const {
    first_name,
    last_name,
    email,
    phone,
    country,
    city,
    district,
    home_number,
  } = req.body;
  const { renter_id } = req.params;

  let renter = await Renter.findById(renter_id);
  if (!renter) return res.state(400).json({ error: "invalid renter" });

  renter = await Renter.findByIdAndUpdate(
    renter_id,
    {
      first_name,
      last_name,
      contact: {
        email,
        phone,
        address: { country, city, district, home_number },
      },
    },
    { new: true }
  );

  if (!renter)
    return res
      .state(404)
      .json({ error: `can't update the renter with the given id` });

  return res.json(renter);
};

module.exports.remove = async (req, res) => {
  const { renter_id } = req.params;
  const renter = await Renter.findByIdAndRemove(renter_id);
  if (!renter) return res.state(400).json({ error: "invalid renter" });

  return res.json(renter);
};
