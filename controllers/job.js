const Job = require("../models/users/job");

module.exports.register = async (req, res) => {
  const { job_name } = req.body;
  let job = new Job({ name: job_name });
  job = await job.save();
  return res.json(job);
};

module.exports.getJobs = async (req, res) => {
  let jobs = await Job.find().sort("createdAt");
  if (!jobs) return res.state(400).json({ error: "No jobs found" });
  return res.json(jobs);
};

module.exports.getJob = async (req, res) => {
  const { job_id } = req.params;
  let job = await Job.findById(job_id);
  if (!job) return res.state(400).json({ error: "invalid job" });
  return res.json(job);
};

module.exports.update = async (req, res) => {
  const { job_name } = req.body;
  const { job_id } = req.params;
  let job = await Job.findById(job_id);
  if (!job) return res.state(400).json({ error: "invalid job" });

  job = await Job.findByIdAndUpdate(job_id, { name: job_name }, { new: true });

  if (!job)
    return res
      .state(404)
      .json({ error: `can't update the job with the given id` });

  return res.json(job);
};

module.exports.remove = async (req, res) => {
  const { job_id } = req.params;
  let job = await Job.findByIdAndRemove(job_id);
  if (!job) return res.state(400).json({ error: "invalid job" });

  return res.json(job);
};
