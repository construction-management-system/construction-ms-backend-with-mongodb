const Employee = require("../models/users/employee");
const Job = require("../models/users/job");

module.exports.register = async (req, res) => {
  const {
    first_name,
    last_name,
    ssn,
    photo,
    hire_date,
    email,
    phone,
    country,
    city,
    district,
    home_number,
    role,
    shift,
    job_id,
    salary,
    contract_file,
    CV_link,
  } = req.body;

  let job = await Job.findById(job_id);
  if (!job) return res.state(400).json({ error: "invalid job" });

  let employee = new Employee({
    first_name,
    last_name,
    ssn,
    photo,
    hire_date,
    contact: {
      email,
      phone,
      address: { country, city, district, home_number },
    },
    role,
    shift,
    job_id,
    salary,
    contract_file,
    CV_link,
  });

  employee = await employee.save();

  return res.json(employee);
};

module.exports.getEmployees = async (req, res) => {
  let employees = await Employee.find().sort("updatedAt");
  if (!employees) return res.state(400).json({ error: "No employees found" });
  return res.json(employees);
};

module.exports.getEmployee = async (req, res) => {
  const { employee_id } = req.params;
  let employee = await Employee.findById(employee_id);
  if (!employee) return res.state(400).json({ error: "invalid employee" });
  return res.json(employee);
};

module.exports.update = async (req, res) => {
  const {
    first_name,
    last_name,
    ssn,
    photo,
    hire_date,
    email,
    phone,
    country,
    city,
    district,
    home_number,
    role,
    shift,
    job_id,
    salary,
    contract_file,
    CV_link,
  } = req.body;
  const { employee_id } = req.params;

  const job = await Job.findById(job_id);
  if (!job) return res.state(400).json({ error: "invalid job" });

  let employee = await Job.findById(employee_id);
  if (!employee) return res.state(400).json({ error: "invalid employee" });

  employee = await Employee.findByIdAndUpdate(
    employee_id,
    {
      first_name,
      last_name,
      ssn,
      photo,
      hire_date,
      contact: {
        email,
        phone,
        address: { country, city, district, home_number },
      },
      role,
      shift,
      job_id,
      salary,
      contract_file,
      CV_link,
    },
    { new: true }
  );

  if (!employee)
    return res
      .state(404)
      .json({ error: `can't update the employee with the given id` });

  return res.json(employee);
};

module.exports.remove = async (req, res) => {
  const { employee_id } = req.params;
  const employee = await Employee.findByIdAndRemove(employee_id);
  if (!employee) return res.state(400).json({ error: "invalid employee" });

  return res.json(employee);
};
