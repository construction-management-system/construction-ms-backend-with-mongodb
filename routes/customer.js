const express = require("express");
const { body, param } = require("express-validator");

const {
  register,
  update,
  remove,
  getCustomer,
  getCustomers,
} = require("../controllers/customer");

const router = express.Router();

// register route
router.post(
  "/",
  [
    body("city")
      .trim()
      .optional(true)
      .isAlpha()
      .withMessage("city required alphabats"),
    body("district")
      .trim()
      .optional(true)
      .isAlpha()
      .withMessage("district required alphabats"),
    body("home_number")
      .trim()
      .optional(true)
      .isAlphanumeric()
      .withMessage("home required Alphanumeric value"),
    body("phone").trim().isNumeric().withMessage("phone number most be number"),
    body("email")
      .trim()
      .optional(true)
      .toLowerCase()
      .isEmail()
      .withMessage("email must be valid")
      .normalizeEmail(),
    body("first_name")
      .trim()
      .notEmpty()
      .withMessage("first name is required")
      .isAlpha()
      .withMessage("first name required alphabats"),
    body("last_name")
      .trim()
      .notEmpty()
      .withMessage("last name is required")
      .isAlpha()
      .withMessage("last name required alphabats"),
    body("ssn")
      .trim()
      .notEmpty()
      .withMessage("ssn most be requird")
      .isNumeric()
      .withMessage("ssn most be number"),
    body("photo")
      .trim()
      .optional(true)
      .isURL()
      .withMessage("photo is required"),
  ],
  register
);

// SEARCH CUSTOMER
router.get(
  "/",
  [
    body("user_id")
      .trim()
      .notEmpty()
      .withMessage("user_id is requird")
      .isNumeric()
      .withMessage("user_id most be number"),
    body("search")
      .trim()
      .notEmpty()
      .withMessage("string is required")
      .isString()
      .withMessage("name required alphabats"),
  ],
  getCustomers
);

// get customer
router.get("/:user_id", getCustomer);

// update route
router.put(
  "/:customer_id",
  [
    body("user_id")
      .trim()
      .notEmpty()
      .withMessage("user_id is requird")
      .isNumeric()
      .withMessage("user_id most be number"),
    param("customer_id")
      .trim()
      .notEmpty()
      .withMessage("customer is requird")
      .isNumeric()
      .withMessage("customer most be number"),
    body("city")
      .trim()
      .optional(true)
      .isAlpha()
      .withMessage("city required alphabats"),
    body("district")
      .trim()
      .optional(true)
      .isAlpha()
      .withMessage("district required alphabats"),
    body("home number")
      .trim()
      .optional(true)
      .isAlphanumeric()
      .withMessage("home required Alphanumeric value"),
    body("map_location")
      .trim()
      .optional(true)
      .isString()
      .withMessage("location accept string value"),
    body("phone").trim().isNumeric().withMessage("phone number most be number"),
    body("email")
      .trim()
      .optional(true)
      .toLowerCase()
      .isEmail()
      .withMessage("email must be valid")
      .normalizeEmail(),
    body("first_name")
      .trim()
      .notEmpty()
      .withMessage("first name is required")
      .isAlpha()
      .withMessage("first name required alphabats"),
    body("last_name")
      .trim()
      .notEmpty()
      .withMessage("last name is required")
      .isAlpha()
      .withMessage("last name required alphabats"),
    body("ssn")
      .trim()
      .notEmpty()
      .withMessage("ssn most be requird")
      .isNumeric()
      .withMessage("ssn most be number"),
    body("photo")
      .trim()
      .optional(true)
      .isURL()
      .withMessage("photo is required"),
    body("registeration_date")
      .trim()
      .optional(true)
      .isDate()
      .withMessage("date"),
  ],
  update
);

// delete route
router.delete(
  "/:customer_id",
  [
    param("customer_id")
      .trim()
      .notEmpty()
      .withMessage("customer is requird")
      .isNumeric()
      .withMessage("customer_id most be number"),
    body("user_id")
      .trim()
      .notEmpty()
      .withMessage("user_id is requird")
      .isNumeric()
      .withMessage("user_id most be number"),
  ],
  remove
);

module.exports = router;
