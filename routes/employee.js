const express = require("express");
const { body } = require("express-validator");
const {
  register,
  remove,
  update,
  getLimited,
} = require("../controllers/employee");

const router = express.Router();

// register route
router.post("/", register);

// get limeted employee
router.get("/:user_id", getLimited);

// update route
router.put("/:employee_id", update);

// delete route
router.delete("/:employee_id", remove);

module.exports = router;
