const express = require("express");
const { body, param } = require("express-validator");

const {
  register,
  update,
  remove,
  getRenter,
  getRenters,
} = require("../controllers/renter");

const router = express.Router();

// register route
router.post(
  "/",
  [
    body("first_name")
      .trim()
      .notEmpty()
      .withMessage("first name is required")
      .isAlpha()
      .withMessage("first name required alphabats"),
    body("last_name")
      .trim()
      .notEmpty()
      .withMessage("last name is required")
      .isAlpha()
      .withMessage("last name required alphabats"),
    body("city")
      .trim()
      .optional(true)
      .isAlpha()
      .withMessage("city required alphabats"),
    body("district")
      .trim()
      .optional(true)
      .isAlpha()
      .withMessage("district required alphabats"),
    body("home number")
      .trim()
      .optional(true)
      .isAlphanumeric()
      .withMessage("home required Alphanumeric value"),
    body("map_location")
      .trim()
      .optional(true)
      .isString()
      .withMessage("location accept string value"),
    body("phone").trim().isNumeric().withMessage("phone number most be number"),
    body("email")
      .trim()
      .optional(true)
      .toLowerCase()
      .isEmail()
      .withMessage("email must be valid")
      .normalizeEmail(),
  ],
  register
);

// get renters
router.get("/", getRenters);

// update route
router.put(
  "/:renter_id",
  [
    param("renter_id")
      .trim()
      .notEmpty()
      .withMessage("renter_id is requird")
      .isNumeric()
      .withMessage("renter_id most be number"),
    body("city")
      .trim()
      .optional(true)
      .isAlpha()
      .withMessage("city required alphabats"),
    body("district")
      .trim()
      .optional(true)
      .isAlpha()
      .withMessage("district required alphabats"),
    body("home number")
      .trim()
      .optional(true)
      .isAlphanumeric()
      .withMessage("home required Alphanumeric value"),
    body("map_location")
      .trim()
      .optional(true)
      .isString()
      .withMessage("location accept string value"),
    body("phone").trim().isNumeric().withMessage("phone number most be number"),
    body("email")
      .trim()
      .optional(true)
      .toLowerCase()
      .isEmail()
      .withMessage("email must be valid")
      .normalizeEmail(),
    body("full_name")
      .trim()
      .notEmpty()
      .withMessage("full name is required")
      .isAlpha()
      .withMessage("full name required alphabats"),
  ],
  update
);

// delete route
router.delete(
  "/:renter_id",
  [
    body("user_id")
      .trim()
      .notEmpty()
      .withMessage("user_id is requird")
      .isNumeric()
      .withMessage("user_id most be number"),
  ],
  remove
);

module.exports = router;
