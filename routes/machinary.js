const express = require('express');
const { body, param } = require('express-validator');
const {
    register,
    update,
    remove,
    getLimited,
    getSearch
} = require('../controllers/machinary');

const router = express.Router();

   // SEARCH MACHINARY
   router.get("/search",
   [
       body('user_id').trim().notEmpty().withMessage('user_id is requird').isNumeric().withMessage('user_id most be number'),
       body('search').trim().notEmpty().withMessage('string is required').isString().withMessage('name required alphabats')
   ], getSearch);

// register route
router.post("/",
    [
        body('user_id').trim().notEmpty().withMessage('user_id is requird').isNumeric().withMessage('user_id most be number'),
        body('name').trim().notEmpty().withMessage('name is required').isAlpha().withMessage('name required alphabats'),
        body('model').trim().optional(true).isString().withMessage('model required alphabats, digits...'),
        body('price').trim().notEmpty().withMessage('price is requird').isNumeric().withMessage('price most be number'),
        body ('registeration_date').trim().optional(true).isDate().withMessage('date'),
        body('documents').trim().optional(true).isString().withMessage('add documents'),
    ], register);

 

// get machinary
router.get("/:user_id",
    [
        param('user_id').trim().notEmpty().withMessage('user_id is requird').isNumeric().withMessage('user_id most be number'),
    ], getLimited);



// update route
router.put("/:machinary_id",
    [
        body('user_id').trim().notEmpty().withMessage('user_id is requird').isNumeric().withMessage('user_id most be number'),
        param('machinary_id').trim().notEmpty().withMessage('machinary_id is requird').isNumeric().withMessage('machinary_id most be number'),
        body('name').trim().notEmpty().withMessage('name is required').isAlpha().withMessage('name required alphabats'),
        body('model').trim().optional(true).isString().withMessage('model required alphabats, digits...'),
        body('price').trim().notEmpty().withMessage('price is requird').isNumeric().withMessage('price most be number'),
        body ('registeration_date').trim().optional(true).isDate().withMessage('date'),
        body('documents').trim().optional(true).isString().withMessage('add documents'),
    ], update);

// delete route
router.delete("/:machinary_id",
    [
        body('user_id').trim().notEmpty().withMessage('user_id is requird').isNumeric().withMessage('user_id most be number'),
        param('machinary_id').trim().notEmpty().withMessage('machinary_id is requird').isNumeric().withMessage('machinary_id most be number'),
    ], remove);

module.exports = router;