const express = require('express');
const { body, param } = require('express-validator');

const {
    register,
    getLimited,
    update,
    remove,
    getSearch
} = require('../controllers/equipment');

const router = express.Router();

// register route
router.post('/',
    [
        body('user_id').trim().notEmpty().withMessage('user_id is requird').isNumeric().withMessage('user_id most be number'),
        body('name').trim().notEmpty().withMessage('name is required').isAlpha().withMessage('name required alphabats'),
        body('description').trim().optional(true).isString().withMessage('description required alphabats'),
        body('registeration_date').trim().optional(true).isDate().withMessage('date required'),
        body('price').trim().notEmpty().withMessage('price is requird').isNumeric().withMessage('price most be number'),
        body('count_number').trim().notEmpty().withMessage('quantity is requird').isNumeric().withMessage('quantity most be number'),
    ], register);

// SEARC EQUIPMENT
router.get("/search",
    [
        body('user_id').trim().notEmpty().withMessage('user_id is requird').isNumeric().withMessage('user_id most be number'),
        body('search').trim().notEmpty().withMessage('string is required').isString().withMessage('name required alphabats')
    ], getSearch);

// get machinary
router.get("/:user_id",
    [
        param('user_id').trim().notEmpty().withMessage('user_id is requird').isNumeric().withMessage('user_id most be number'),
    ], getLimited);

// update route
router.put("/:equipment_id",
[
    body('user_id').trim().notEmpty().withMessage('user_id is requird').isNumeric().withMessage('user_id most be number'),
    param('equipment_id').trim().notEmpty().withMessage('equipment_id is requird').isNumeric().withMessage('equipment_id most be number'),
    body('name').trim().notEmpty().withMessage('name is required').isAlpha().withMessage('name required alphabats'),
    body('description').trim().optional(true).isString().withMessage('description required alphabats'),
    body('registeration_date').trim().optional(true).isDate().withMessage('date required'),
    body('price').trim().notEmpty().withMessage('price is requird').isNumeric().withMessage('price most be number'),
    body('count_number').trim().notEmpty().withMessage('quantity is requird').isNumeric().withMessage('quantity most be number'),
], update);

// delete route
router.delete("/:equipment_id",
    [
        param('equipment_id').trim().notEmpty().withMessage('equipment_id is requird').isNumeric().withMessage('equipment_id most be number'),
        body('user_id').trim().notEmpty().withMessage('user_id is requird').isNumeric().withMessage('user_id most be number'),
    ], remove);

module.exports = router;

