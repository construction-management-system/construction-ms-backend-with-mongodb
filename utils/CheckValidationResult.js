const { validationResult } = require("express-validator");

module.exports = (req) => {
  const result = validationResult(req);
  if (!result.isEmpty()) {
    return result.errors.map((error) => {
      return { message: error.msg, parameter: error.param };
    });
  }
};
