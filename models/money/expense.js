const mongoose = require("mongoose");
require("mongoose-type-url");

const Schema = mongoose.Schema();

const expenseSchema = new Schema(
  {
    name: {
      type: String,
      require: true,
    },
    description: {
      type: String,
    },
    date: {
      type: Date,
      default: Date.now,
    },
    amount: {
      type: Number,
      default: 0,
    },
    billPhoto: {
      type: mongoose.SchemaTypes.Url,
    },
    taskId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Task",
    },
    type: {
      type: String,
      require: true,
      enum: ["task", "office"],
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Expense", expenseSchema);
