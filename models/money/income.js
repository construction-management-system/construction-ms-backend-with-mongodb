const mongoose = require("mongoose");

const Schema = mongoose.Schema();

const incomeSchema = new Schema(
  {
    name: {
      type: String,
      require: true,
    },
    description: {
      type: String,
    },
    date: {
      type: Date,
      default: Date.now,
    },
    type:{
      type: String,
      require: true,
      enum:['project', 'capital', 'office' ]
    },
    amount: {
      type: Number,
      default: 0,
    },
    projectId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Project",
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Income", incomeSchema);
