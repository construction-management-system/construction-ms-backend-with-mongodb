const mongoose = require("mongoose");

const Schema = mongoose.Schema();

const rentedSchema = new Schema(
  {
    name: {
      type: String,
      require: true,
    },
    description: {
      type: String,
    },
    renterId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Renter",
      require: true,
    },
    taskId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Task",
    },
    deadline: {
      startDate: { type: Date, default: Date.now },
      endDate: { type: Date, require: true },
    },
    cost: {
      type: Number,
      default: 0,
    },
    count: {
      type: Number,
      default: 0,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Rented", rentedSchema);
