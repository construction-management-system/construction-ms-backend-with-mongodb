const mongoose = require("mongoose");
require("mongoose-type-url");

const Schema = mongoose.Schema();

const machinarySchema = new Schema(
  {
    name: {
      type: String,
      require: true,
    },
    model: {
      type: String,
    },
    price: {
      type: Number,
      require: true,
    },
    document: {
      type: mongoose.SchemaTypes.Url,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Machinary", machinarySchema);
