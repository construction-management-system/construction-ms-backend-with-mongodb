const mongoose = require("mongoose");

const Schema = mongoose.Schema();

const subTaskSchema = new Schema(
  {
    name: {
      type: String,
      require: true,
    },
    description: {
      type: String,
    },
    taskId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Task",
      require: true,
    },
    deadline: {
      startDate: { type: Date, default: Date.now },
      endDate: { type: Date,  },
    },
    state: {
      type: String,
      lowercase: true,
      trim: true,
      enum: ["active", "deactive", "todo", "done"],
      require: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("SubTask", subTaskSchema);
