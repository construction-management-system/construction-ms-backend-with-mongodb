const mongoose = require("mongoose");
require("mongoose-type-url");

const Schema = mongoose.Schema();

const projectTypeSchema = new Schema(
  {
    name: {
      type: String,
      require: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("ProjectType", projectTypeSchema);
