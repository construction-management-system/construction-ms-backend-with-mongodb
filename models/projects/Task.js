const mongoose = require("mongoose");

const Schema = mongoose.Schema();

const TaskSchema = new Schema(
  {
    name: {
      type: String,
      require: true,
    },
    description: {
      type: String,
    },
    teams: [
      {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Team",
        require: true,
      },
    ],
    projectId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Project",
      require: true,
    },
    deadline: {
      startDate: { type: Date, default: Date.now },
      endDate: { type: Date, require: true },
    },
    progress: {
      type: Number,
      default: 0,
      max: 100,
      min: 0,
    },
    state: {
      type: String,
      lowercase: true,
      trim: true,
      enum: ["active", "deactive", "todo", "done"],
      require: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Task", TaskSchema);
