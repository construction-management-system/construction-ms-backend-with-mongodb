const mongoose = require("mongoose");
require("mongoose-type-url");

const Schema = mongoose.Schema;

const projectSchema = new Schema(
  {
    name: {
      type: String,
      unique: true,
      require: true,
    },
    managerID: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Employee",
      require: true,
    },
    supervisorID: [
      {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Employee",
      },
    ],
    deadline: {
      startDate: { type: Date, default: Date.now },
      endDate: { type: Date, require: true },
    },
    state: {
      type: String,
      lowercase: true,
      trim: true,
      enum: ["active", "todo", "done", "deactive"],
      require: true,
    },
    projectTypeID: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "ProjectType",
      require: true,
    },
    customers: [
      {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Customer",
        require: true,
      },
    ],
    progress: {
      type: Number,
      default: 0,
      max: 100,
      min: 0,
    },
    address: {
      country: String,
      city: String,
      district: String,
      street: String,
      homeNumber: String,
      location: {
        latitude: String,
        longtitude: String,
      },
    },
    cost: {
      type: Number,
      require: true,
    },
    contractFile: {
      type: mongoose.SchemaTypes.Url,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Project", projectSchema);
