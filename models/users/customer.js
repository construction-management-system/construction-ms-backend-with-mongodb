const mongoose = require("mongoose");
require("mongoose-type-url");

const Schema = mongoose.Schema;

const customerSchema = new Schema(
  {
    first_name: {
      type: String,
      require: true,
    },
    last_name: {
      type: String,
      require: true,
    },
    ssn: {
      type: Number,
      require: true,
      unique: true,
    },
    photo: {
      type: mongoose.SchemaTypes.Url,
    },
    contact: {
      email: { type: String, require: true, unique: true },
      phone: { type: String, unique: true },
      address: {
        country: String,
        city: String,
        district: String,
        home_number: String,
      },
    },
    contract_file: {
      type: mongoose.SchemaTypes.Url,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Customer", customerSchema);
