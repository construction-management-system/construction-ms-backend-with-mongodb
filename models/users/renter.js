const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const renterSchema = new Schema(
  {
    first_name: {
      type: String,
      require: true,
    },
    last_name: {
      type: String,
      require: true,
    },
    contact: {
      email: { type: String },
      phone: { type: String, unique: true, require: true },
      address: {
        country: String,
        city: String,
        district: String,
        home_number: String,
      },
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Renter", renterSchema);
