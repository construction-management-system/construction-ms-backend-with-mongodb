const mongoose = require("mongoose");

const Schema = mongoose.Schema();

const jobSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      require: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Job", jobSchema);
