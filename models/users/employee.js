const mongoose = require("mongoose");
require("mongoose-type-url");

const Schema = mongoose.Schema();

const employeeSchema = new Schema({
  first_name: {
    type: String,
    require: true,
  },
  last_name: {
    type: String,
    require: true,
  },
  ssn: {
    type: Number,
    require: true,
    unique: true,
  },
  photo: {
    type: mongoose.SchemaTypes.Url,
  },
  hire_date: { type: Date, default: Date.now },
  contact: {
    email: { type: String, unique: true, require: true },
    phone: { type: String, unique: true },
    address: {
      country: String,
      city: String,
      district: String,
      home_number: String,
    },
  },
  role: {
    type: String,
    require: true,
    enum: ["admin", "secretory", "manager", "supervoiser", "customer"],
  },
  shift: {
    type: String,
    require: true,
    enum: ["full time", "part time", "freelancer", "night shift"],
  },
  job_id: {
    type: mongoose.SchemaTypes.ObjectId,
    ref: "Job",
    require: true,
  },
  salary: {
    type: Number,
    default: 0,
  },
  contract_file: {
    type: mongoose.SchemaTypes.Url,
  },
  CV_link: {
    type: mongoose.SchemaTypes.Url,
  },
});

module.exports = mongoose.model("Employee", employeeSchema);
