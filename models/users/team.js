const mongoose = require("mongoose");

const Schema = mongoose.Schema();

const teamSchema = new Schema(
  {
    name: {
      type: String,
      require: true,
    },
    description: {
      type: String,
    },
    teamLeaderId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Employee",
      require: true,
    },
    teamMembers: [
      { type: mongoose.SchemaTypes.ObjectId, ref: "Employee", require: true },
    ],
    deadline: {
      startDate: { type: Date, default: Date.now },
      endDate: { type: Date },
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Team", teamSchema);
