// third-party imports
const express = require("express");
// project imports
const errorHandler = require("../middlewares/errorHandler");
const job_router = require("../routes/job");
const renter_router = require("../routes/renter");
const customer_router = require("../routes/customer");

module.exports = (app) => {
  // middlewares
  app.use(express.urlencoded({ extended: false }));
  app.use(express.json());

  // routes
  // app.use("/login", auth_router);
  // app.use("/employee", employee_router);
  // app.use("/role", role_router);
  app.use("/job", job_router);
  app.use("/renter", renter_router);
  app.use("/customer", customer_router);
  // app.use("/shift", shift_router);

  // express error handler middleware
  app.use(errorHandler);
};
