const {checkAuthHeader} = require("../utils/checkAuthHeader");

module.exports.isAuth = (req, res, next) => {
  const authHeader = checkAuthHeader(req.headers);
  if (authHeader.error) return res.json(authHeader);

  next();
};
