// third-party imports
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// intializing app
const app = express();

// database connection
mongoose.connect("mongodb://localhost/construction-ms", {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
});

require("./startup/logging")();
app.use(cors());
require("./startup/routes")(app);

// set port
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server running on http://localhost:${PORT}`);
});
